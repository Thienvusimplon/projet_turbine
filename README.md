# Représentation des Diagnostics de performance énergétique (DPE) sur l’Agglomération grenobloise

#### Page web déployée
https://thienvusimplon.gitlab.io/projet_turbine/

#### Contenu du dépôt GitLab

Dossiers : 
-   code/ : contient les scripts python du projet
-   data/ : contient le csv livrable pour la plateforme ODMetro et un fichier cache.csv nécéssaire pour un script
-   document/ : contient tous les schémas aidant à la réalisation du projet

Fichiers :
-   Pipfile et Pipfile.lock : nécéssaire pour l'installation  de l'environement virtuel
-   .gitlab-ci.yml : fichier servant pour le deploiment en ligne
-   .gitignore : contient les fichiers/dossiers non nécéssaire pour le fonctionnement correct du dépôt 
-   presentation_finale.ipynb : fichier jupyter notebook contenant la page web déployée

#### Installation de l'environement virtuel
```bash
pipenv install
```
#### Lancement de l'environnement virtuel
```bash
pipenv shell
```
#### Démarrage de jupyter notebook
```bash
jupyter notebook
```
Une fois dans le jupyter notebook, lancez le fichier nommé presentation_finale.ipynb.  
Exécutez les cellules une par une avec la commande suivante :
```bash
shift + entrée
```
#### Mise à jour de la page déployer 
Afin de mettre à jour la page déployée, éxécutez les commandes suivantes :
```bash
git add presentation_finale.ipynb
git commit -m 'presentation_finale'
git push
```
Lorsque vous avez push sur Gitlab, attendre environ 5mn pour que la page puisse se déployer.
