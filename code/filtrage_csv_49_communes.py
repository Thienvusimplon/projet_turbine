import pandas as pd
def filtrage_csv_49_communes(df,code_insee,nom_commune):
    df['code_insee_commune'] = df['code_insee_commune'].fillna(0).astype(int,errors='ignore')
    #df = df[df['code_insee_commune'].isin(code_insee)]
    df['commune'] = df['commune'].str.lower()
    df = df[df['commune'].isin(nom_commune)]
    df = df.reset_index()
    df = df.drop(['index'], axis=1)
    #print(len(df2),"lignes sur",len(df),"(",round(len(df2)/len(df)*100,2),"%) ont été chargé après application des filtres code insee et nom de commune")
    #df = df.dropna(subset = ["code_insee_commune"], inplace=False)
    df.to_csv("./data/49_communes.csv")

    return df
