#!/usr/bin/env python
# coding: utf-8

# In[2]:


import requests

def code_insee():
    #Création liste code insee
    r = requests.get("https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json")
    data = r.json()
    nb_code_insee = len(data["features"])
    code_insee = []
    
    for i in range(nb_code_insee):
        code_insee.append(data["features"][i]["properties"]["code_insee"])
    
    return code_insee


# In[ ]:




