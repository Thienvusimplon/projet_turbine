import pandas as pd

def chargement_xlxs() :
    dfx = pd.read_excel("./data/insee_rp_hist_1968.xlsx")
    dfx['Unnamed: 1'] = dfx['Unnamed: 1'].str.lower()
    
    return dfx
