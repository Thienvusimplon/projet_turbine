import pandas as pd

def taux_couverture(df2,nom_commune,code_insee,dfx):
    nb_dpe_commune = df2.groupby(["commune"])["classe_consommation_energie"].count()
    csv = {'nom': [], 'pourcentage':[]}
    for nom,code in zip(nom_commune,code_insee):
        if nom == "le fontanil-cornillon":
            nom = "fontanil-cornillon"
        try : 
            #print(nom,code)
            oki = dfx.loc[(dfx["Unnamed: 1"]== nom) & (dfx["Unnamed: 2"]== "2017") & (dfx["Observatoire des territoires - ANCT"]== code)]
            if nom == "fontanil-cornillon":
                nom = "le fontanil-cornillon"
            #print("nb_dpe_commune[line]",nb_dpe_commune[line],"oki[Unnamed: 3]",oki["Unnamed: 3"])
            taux = int(int(nb_dpe_commune[nom])/int(oki["Unnamed: 3"])*100)
            csv["nom"].append(nom)
            csv["pourcentage"].append(taux)
        except (KeyError):
            print(line)
    csv = pd.DataFrame(csv)

    #csv = csv.where(csv["pourcentage"] < 100)
    #csv = csv.dropna(subset = ["pourcentage"], inplace=False)

    return csv
