import folium
import folium.plugins
import numpy as np
import requests
import json
import pandas as pd
from shapely.geometry import Point
import geopandas as gpd


def programme_carto_cluster(df2):
    def api(description):
        lien = "https://api-adresse.data.gouv.fr/search/?q="
        i = 1
        for elm in description:
            if i == 1:
                lien = f"{lien}{elm}"
                i += 1
            elif i == len(description):
                lien = f"{lien}&citycode={elm}"
                i += 1
            else:
                lien =f"{lien}+{elm}"
                i += 1
        return lien

    def lat_long(geocode):
        response = requests.get(geocode)
        data = response.json()
        try :
            temp = data['features'][0]['geometry']['coordinates']
            lat_long = ((temp[1],temp[0]))
            return lat_long
        except (IndexError):
            return False
                
    nil = gpd.read_file("https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json")
    polygon = nil.geometry.unary_union
    
    def in_or_out(df2,polygon):
        recherche = []
        
        for _, elm in df2.iterrows():
            test = []
            test.append((elm["longitude"],elm["latitude"]))
            p3 = Point(test)
            dedans = p3.within(polygon)
            if dedans == True:
                pass
            else:
                recherche.append(_)
                
        print(len(recherche),"point mal placé sur",len(df2),"(",round(len(recherche)/len(df2)*100,2),"%)")
        return recherche
    
    recherche = in_or_out(df2,polygon)
    
    classement_dpe = df2["classe_consommation_energie"].unique()
    classe = []
    coordonnees = []

    for elm in classement_dpe:
        classe.append(elm)
    classe.sort()
    classe.pop()

    carte = folium.Map(location=(45.188529, 5.724524), zoom_start=11)
    
    df_cache = pd.read_csv("./data/cache.csv")
    #df_cache = pd.DataFrame()
    couleurs_dpe = {
    "A": "blue",
    "B": "orange",
    "C": "red",
    "D": "lightblue",
    "E": "green",
    "F": "beige",
    "G": "purple"
    }
    interieur = 0
    exterieur = 0
    liste_exterieur = []
    
    df3 = df2.loc[recherche]
    for _, row in df3.iterrows():
        description = []
        if _ in recherche:
            if type(row["numero_rue"])==float and type(row["type_voie"])==float and type(row["nom_rue"])==str:
                if row["code_insee_commune"]==float:
                    row["code_insee_commune"]=int(row["code_insee_commune"])
                elif row["code_insee_commune"]==str:
                    row["code_insee_commune"]=row["code_insee_commune"].replace(" ", "")
                description.append([str(row["nom_rue"]).split(),row["code_insee_commune"]])                
                geocode = api(description)
                if geocode not in df_cache.columns: 
                    calcul = lat_long(geocode)
                    if calcul != False:
                        df2.at[_, 'latitude'] = calcul[0]
                        df2.at[_, 'longitude'] = calcul[1]
                        df_cache[geocode]=calcul
                else:
                    df2.at[_, 'latitude'] = df_cache.iloc[0][geocode]
                    df2.at[_, 'longitude'] = df_cache.iloc[1][geocode]
            elif type(row["numero_rue"])==float and type(row["type_voie"])==str and type(row["nom_rue"])==str:
                if row["code_insee_commune"]==float:
                    row["code_insee_commune"]=int(row["code_insee_commune"])
                elif row["code_insee_commune"]==str:
                    row["code_insee_commune"]=row["code_insee_commune"].replace(" ", "")
                description.append([str(row["type_voie"]).split(),str(row["nom_rue"]).split(),row["code_insee_commune"]])
                geocode = api(description)               
                if geocode not in df_cache.columns:
                    calcul = lat_long(geocode)
                    if calcul != False:
                        df2.at[_, 'latitude'] = calcul[0]
                        df2.at[_, 'longitude'] = calcul[1]
                        df_cache[geocode]=calcul
                else:
                    df2.at[_, 'latitude'] = df_cache.iloc[0][geocode]
                    df2.at[_, 'longitude'] = df_cache.iloc[1][geocode]
            elif type(row["numero_rue"])==str and type(row["type_voie"])==float and type(row["nom_rue"])==str:
                if row["code_insee_commune"]==float:
                    row["code_insee_commune"]=int(row["code_insee_commune"])
                elif row["code_insee_commune"]==str:
                    row["code_insee_commune"]=row["code_insee_commune"].replace(" ", "")
                description.append([str(row["numero_rue"]).split(),str(row["nom_rue"]).split(),row["code_insee_commune"]])
                geocode = api(description)
                if geocode not in df_cache.columns: 
                    calcul = lat_long(geocode)
                    if geocode not in df_cache.columns: 
                        calcul = lat_long(geocode)
                        if calcul != False:
                            df2.at[_, 'latitude'] = calcul[0]
                            df2.at[_, 'longitude'] = calcul[1]
                            df_cache[geocode]=calcul
                else:
                    df2.at[_, 'latitude'] = df_cache.iloc[0][geocode]
                    df2.at[_, 'longitude'] = df_cache.iloc[1][geocode]
            elif type(row["numero_rue"])==str and type(row["type_voie"])==str and type(row["nom_rue"])==str:
                if row["code_insee_commune"]==float:
                    row["code_insee_commune"]=int(row["code_insee_commune"])
                elif row["code_insee_commune"]==str:
                    row["code_insee_commune"]=row["code_insee_commune"].replace(" ", "")
                description.append([str(row["numero_rue"]),str(row["type_voie"]),str(row["nom_rue"]).split(),row["code_insee_commune"]])               
                geocode = api(description)
                if geocode not in df_cache.columns: 
                    calcul = lat_long(geocode)
                    if calcul != False:
                        df2.at[_, 'latitude'] = calcul[0]
                        df2.at[_, 'longitude'] = calcul[1]
                        df_cache[geocode]=calcul
                else:
                    df2.at[_, 'latitude'] = df_cache.iloc[0][geocode]
                    df2.at[_, 'longitude'] = df_cache.iloc[1][geocode]

            p3 = Point((df2.iloc[_]["longitude"],df2.iloc[_]["latitude"]))
            dedans = p3.within(polygon)
            if dedans == True:
                interieur +=1
            else:
                liste_exterieur.append(_)               
                exterieur +=1
  
    df4 = df2.drop(liste_exterieur)
    print(interieur,"points replacé correctement sur",len(recherche),"(",round(interieur/len(recherche)*100,2),"%)")
    #print(exterieur,"points toujours mal placé sur",len(recherche),"(",round(exterieur/len(recherche)*100,2),"%)")
    print(exterieur,"point mal placé sur",len(df2),"(",round(exterieur/len(df2)*100,2),"%)")
    df_cache.to_csv("./data/cache.csv")
    df2.to_csv("./data/49_communes.csv")
    
    for elm in classe:
        numero_dans_liste = 0
        carto = df4.loc[df4['classe_consommation_energie'] == elm]
        carto = carto.dropna(subset = ["latitude"], inplace=False)
        carto = carto.dropna(subset = ["longitude"], inplace=False)
        coordonnees = []
        popups = []
        couleur = couleurs_dpe[elm]
        icones = []
        layer_color = f"{elm} : {couleur}"
        for _, row in carto.iterrows():
            texte = f"classement DPE : {elm} \n consommation énergie en kWh : {row['consommation_energie']}"
            coordonnees.append([row["latitude"],row["longitude"]])
            popups.append([texte])
            icones.append(folium.Icon(color=couleur))
        folium.plugins.MarkerCluster(
            name=layer_color,
            locations=coordonnees,
            highlight=True,
            popups=popups,
            icons=icones,
        ).add_to(carte)

    folium.LayerControl().add_to(carte)
    
    return carte
