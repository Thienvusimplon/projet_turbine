# Représentation des Diagnostics de performance énergétique (DPE) sur l’Agglomération grenobloise

## 1)  Le jeu de données localisé, au format requis par le CKAN.

Scripts utilisés :
-   chargement_data.py
-   code_insee.py
-   nom_commune.py
-   filtrage_csv_49_communes.py

Nous avons à notre disposition un fichier csv [téléchargeable](https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/raw) sur le portail de l'ADEME. Il contient environ 170 000 lignes de données, beaucoup nous n'intéresse pas pour notre étude. Afin de garder ce qui est pertinent, un filtrage sera effectué à l'aide d'un fichier [geojson](https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json).

![<Schéma chargement du jeu de donnée>](https://i.ibb.co/4fL6DC7/chargement.jpg)  

Un filtrage par les codes Insee a été fait initialement cependant plusieurs villes étaient rattachées au même code Insee ce qui est pas normal.  
Une différente approche a été adopté. Celle du filtrage par les noms de commune.  

Voici un example de code Insee et nom de commune.

![<Example nettoyage>](https://i.ibb.co/8YLx9cP/nettoyage.jpg)

1er cas : code insee bon, nom de commune bon  
2ème cas : code insee bon, nom de commune faux  
4ème cas : code insee bon, nom de commune mal orthographié  

Un traitement a été effecté sur le nom de commune. Tous les noms de commune on été formaté en minuscule afin d'éviter les erreurs de case comme Grenoble, grenoble ou grEnoble etc...  

Une fois le filtrage terminé, un fichier nommé 49_communes.csv est crée dans le dossier data qui pourra être utilisé sur la plateforme [data.metropolegrenoble.fr](https://data.metropolegrenoble.fr/ckan/dataset)  

## 2)  Cartographie taux de couverture.

Scripts utilisés :
-   chargement_xlxs.py
-   taux_couverture.py
-   carto_taux_couverture.py

Afin de pouvoir générer la cartographie représentant le taux de couverture par commune, nous avons besoin d'une source de données supplémentaire qui contient le nombre de logement par commune.
Pour calculer le taux de couverture de Grenoble par exemple, un programme ira chercher le nombre de logement disposant d'un DPE dans le csv filtré de l'ADEME puis effectuera un calcul de pourcentage avec le nombre total de logement dans Grenoble.
Lorsque le taux de couverture sera calculé pour toutes les communes, une cartographie sera disponible afin d'avoir une visualisation. Plus le taux de couverture est élevé, plus la zone tend vers le rouge foncé.

![<Schéma taux de couverture>](https://i.ibb.co/pwNnhjh/taux-couverture.png)

## 3)  Cartographie cluster par commune par DPE.

Script utilisé :
-   programme_carto_cluster.py

Chaque diagnostic sera représenté par un point sur une carte de l’agglomération grenobloise. Lorsqu'on dézoome, les points représentant le même DPE seront regroupé pour créer un cluster. Dans notre fichier, certaines longitudes et latitudes sont manquantes. Un programme tentera de récupérer les coordonnées à partir de l'adresse du logement concerné.

![<Schéma carto cluster>](https://i.ibb.co/LN5MV9h/carto-cluster.png)

Avec notre fichier csv correctement filtré, un script récupère les longitudes et latitudes de chaque ligne pour les tester s'ils sont à l'intérieur de l'agglomération grenobloise. Si oui, aucun traitement supplémentaire est nécessaire. Sinon, un script va tenter de retrouver des coordonnées valide si possible en utilisant l'adresse où le diagnostic a été fait. Un nouveau test est effectué sur les nouvelles coordonnées, si elles sont toujours à l'exterieur de l'agglomération alors cette ligne sera supprimé du csv sinon elles seront affichées sur la carte.

## Bibliothèque

### Page web déployée
-   https://thienvusimplon.gitlab.io/projet_turbine/

### Sources de données

-   Diagnostics de performance énergétique (DPE) pour le département 38 (Isère)  
https://data.ademe.fr/datasets/dpe-38  

-   Les communes de la Métropole  
https://data.metropolegrenoble.fr/ckan/dataset/les-communes-de-la-metropole  

-   Nombre de logements  
https://www.observatoire-des-territoires.gouv.fr/nombre-de-logements

### Librairies python utilisées

-   Pandas : https://pandas.pydata.org/docs/user_guide/index.html  
-   Geopandas : https://geopandas.org/docs.html  
-   Altair : https://altair-viz.github.io/  
-   Folium : https://python-visualization.github.io/folium/  
-   Shapely : https://shapely.readthedocs.io/en/stable/manual.html  






