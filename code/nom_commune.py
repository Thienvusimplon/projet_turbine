import requests

def nom_commune():
    #Création liste code insee
    r = requests.get("https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json")
    data = r.json()
    nb_code_insee = len(data["features"])
    nom_commune = []
    
    for i in range(nb_code_insee):
        #print(data["features"][i]["properties"]["nom"].lower(), data["features"][i]["properties"]["code_insee"])
        nom_commune.append(data["features"][i]["properties"]["nom"].lower())
        #code_insee.append(data["features"][i]["properties"]["code_insee"])
    
    return nom_commune