import folium
import altair as alt
import pandas as pd
import folium.plugins
import geopandas as gpd
from geopy.geocoders import Nominatim

def carto_taux_couverture(df2,tc,code_insee, nom_commune):
    geolocator = Nominatim(user_agent="thienvu")

    contour_geojson = gpd.read_file("https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json")
    contour_geojson['nom'] = contour_geojson['nom'].str.lower()
    dataframe_source=contour_geojson.merge(tc,on="nom")

    mymap = folium.Map(location=[45.188529, 5.724524], zoom_start=11,tiles=None)
    folium.TileLayer('OpenStreetMap',name="Light Map",control=False).add_to(mymap)

    choropleth = mymap.choropleth(
     geo_data=dataframe_source,
     name='Contour',
     data=dataframe_source,
     columns=['nom','pourcentage'],
     key_on="feature.properties.nom",
     fill_color='Reds',
     fill_opacity=1,
     line_opacity=0.2,
     legend_name='Taux de couverture en %',
     smooth_factor=0
    )

    style_function = lambda x: {'fillColor': '#ffffff', 
                            'color':'#000000', 
                            'fillOpacity': 0.1, 
                            'weight': 0.1}
    highlight_function = lambda x: {'fillColor': '#000000', 
                                'color':'#000000', 
                                'fillOpacity': 0.50, 
                                'weight': 0.1}

    layer1 = folium.FeatureGroup(name="popups", show=True).add_to(mymap)
    layer2 = folium.FeatureGroup(name="tooltip", show=True).add_to(mymap)
    
    for gps,code,commune in zip(dataframe_source["geometry"],dataframe_source["nom"],dataframe_source["nom"]):
        gj = folium.GeoJson(
        data=gps
        )
        groupement_dpe = df2.groupby(["commune","classe_consommation_energie"])["classe_consommation_energie"].count()
        groupement_ges = df2.groupby(["commune","classe_estimation_ges"])["classe_estimation_ges"].count()
        ges = groupement_ges[code].tolist()
        dpe = groupement_dpe[code].tolist()
        dpe.pop()
        ges.pop()
        index_dpe = groupement_dpe[code].index.tolist()
        index_ges = groupement_ges[code].index.tolist()
        index_dpe.pop()
        index_ges.pop()
        source_dpe = pd.DataFrame(
            {
                'classement': index_dpe,
                'valeur dpe': dpe,
            }
        )
        
        source_ges = pd.DataFrame(
            {
                'classement': index_ges,
                'valeur ges': ges,
            }
        ) 
        
        # create an altair chart, then convert to JSON
        chart_dpe = alt.Chart(source_dpe).mark_bar().encode(x='classement', 
                                                            y='valeur dpe',
                                                            tooltip=["classement","valeur dpe"],
                                                            color='classement').interactive()            
        chart_ges = alt.Chart(source_ges).mark_bar().encode(x='classement', 
                                                            y='valeur ges',
                                                            tooltip=["classement","valeur ges"],
                                                            color='classement').interactive()
        chart = alt.hconcat(chart_dpe, chart_ges)
        
        vis1 = chart.to_json()
              
        gj.add_child(folium.Popup(max_width=750).add_child(folium.VegaLite(vis1, width=520, height=200)))
        gj.add_to(layer1)
    
    map_interractive = folium.features.GeoJson(
    dataframe_source,
    style_function=style_function, 
    control=False,
    highlight_function=highlight_function, 
    tooltip=folium.features.GeoJsonTooltip(
        fields=['nom','pourcentage','code_insee'],
        aliases=['nom_commune:','taux de couverture en %:','code_insee'],
        style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;"),
        )
    )

    layer2.add_child(map_interractive)
    folium.LayerControl().add_to(mymap)
    
    return mymap
